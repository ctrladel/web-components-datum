# Usage

1. `git clone git@gitlab.com:ctrladel/web-components-datum.git`
1. `cd web-components-datum`
1. `ddev start`
1. `ddev composer install`
1. `ddev drush sqlc < db.sql`
1. `ddev drush cr`
1. `ddev drush uli`
